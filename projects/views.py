from django.shortcuts import render
from django.views.generic.list import ListView

# Create your views here.
from projects.models import Project

class ProjectsListView(ListView):
    model = Project
    template_name = "projects/list.html"
    context_object_name = "list_projects"

    # def get_queryset(self):
    #     return Project.objects.filter(members=self.request.user)

    